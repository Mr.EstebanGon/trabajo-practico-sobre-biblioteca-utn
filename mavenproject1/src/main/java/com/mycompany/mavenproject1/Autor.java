package com.mycompany.mavenproject1;

import java.io.Serializable;
import java.util.Date;

public class Autor extends Persona implements Serializable {
    private Date fechaNac;
    private String nacionalidad;

    public Autor(Date fechaNac, String nacionalidad, String nombre) {
        super(nombre);
        this.fechaNac = fechaNac;
        this.nacionalidad = nacionalidad;
    }
    
    

    public void setFechaNac(Date fechaNac) {
        this.fechaNac = fechaNac;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public Date getFechaNac() {
        return fechaNac;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }
    
    
}

