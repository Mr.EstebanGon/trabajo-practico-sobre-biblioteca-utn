package com.mycompany.mavenproject1;

import java.io.Serializable;

public abstract class Persona implements Serializable {
    private String nombre;
 

    public Persona(String nombre) {
        this.nombre = nombre;
    }
    

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }
    
    
}
