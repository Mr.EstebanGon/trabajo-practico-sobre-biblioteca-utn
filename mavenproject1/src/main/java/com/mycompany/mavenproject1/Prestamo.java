package com.mycompany.mavenproject1;

import java.util.Date;

public class Prestamo {
  private Integer idLector;
  private Integer idLibro;
  private Date fechaEgreso;
  private Date fechaIngreso;
  private Multa multa;

    public void setIdLector(Integer idLector) {
        this.idLector = idLector;
    }

    public void setIdLibro(Integer idLibro) {
        this.idLibro = idLibro;
    }

    public void setFechaEgreso(Date fechaEgreso) {
        this.fechaEgreso = fechaEgreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public void setMulta(Multa multa) {
        this.multa = multa;
    }

    public Integer getIdLector() {
        return idLector;
    }

    public Integer getIdLibro() {
        return idLibro;
    }

    public Date getFechaEgreso() {
        return fechaEgreso;
    }

    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public Multa getMulta() {
        return multa;
    }
  
  
  
}
