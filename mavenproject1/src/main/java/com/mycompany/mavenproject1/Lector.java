package com.mycompany.mavenproject1;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;

public class Lector extends Usuario implements Serializable {
    private Date fechaMulta;
    private Integer maxMulta;

    public Lector(String password, String nombre) {
        super(password, nombre);
          //this.fechaMulta = NULL;// .....
        this.maxMulta =3;// O 0
    } 

    public void setFechaMulta(Date fechaMulta) {
        this.fechaMulta = fechaMulta;
    }

    public void setMaxMulta(Integer maxMulta) {
        this.maxMulta = maxMulta;
    }

    public Date getFechaMulta() {
        return fechaMulta;
    }

    public Integer getMaxMulta() {
        return maxMulta;
    }

    @Override
    boolean proceder(Biblioteca biblioteca) {
        char op;
        boolean continuar = true;

        do {
        op = EntradaSalida.leerChar(
                "**********************************************\nFUNCIONES DEL LECTOR\n**********************************************\n"
                        + "1 - Consulta de libro.\n"
                        + "2 - Elegir otro libro.\n"
                        + "3 - Cambiar de Usuario\n"
                        + "4 - Salir del sistema");

        switch (op) {
            case '1':

                break;
            case '2':

                break;
            case '3':
                continuar = true;
                break;
            case '4':
                continuar = false;
                break;
            default:
                EntradaSalida.mostrarString("ERROR: Opcion invalida");
                op = '*';
        } //Fin switch
        if (op >= '1' && op <= '2') {
            try {
                biblioteca.serializar("biblioteca.txt");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    } while (op != '3' && op != '4'); //Fin while

        return continuar;
}
}
