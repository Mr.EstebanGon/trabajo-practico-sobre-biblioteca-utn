package com.mycompany.mavenproject1;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Scanner;


public class Biblioteca implements Serializable {
    private ArrayList<Libro> libros;
    private ArrayList<Usuario> usuarios;
    private ArrayList<Prestamo> prestamos;


    public Biblioteca() {
        libros = new ArrayList<Libro>();
        usuarios = new ArrayList<Usuario>();
        prestamos = new ArrayList<Prestamo>();
    }

    public Biblioteca deSerializar(String s) throws IOException, ClassNotFoundException {
        FileInputStream f = new FileInputStream(s);
        ObjectInputStream o = new ObjectInputStream(f);
        Biblioteca b = (Biblioteca) o.readObject();
        f.close();
        o.close();
        return b;
    }

    public void serializar(String s) throws IOException {
        FileOutputStream f = new FileOutputStream(s);
        ObjectOutputStream o = new ObjectOutputStream(f);
        o.writeObject(this);
        f.close();
        o.close();
    }


    public void PrimeraPantalla() {
        System.out.println("Bienvenido a la Biblioteca");
    }

    public ArrayList<Libro> elegirLibro(Usuario u) {
        Scanner entrada = new Scanner(System.in);
        ArrayList<Libro> libroElegido = null;
        int max = 3;
        int elegido;
        for (int i = 0; i < libros.size(); i++) {
            System.out.println(i);
            System.out.println(libros.get(i).toString());
        }
        if (u != null) // podemos dar una lógica de que multado sea null
        {
            for (int j = 1; j < max; j++)
                System.out.println("Elija el libro" + j);
            elegido = entrada.nextInt();
            libroElegido.add(libros.get(elegido));
            System.out.println("El Libro elegido es: " + libroElegido.toString());
        }
        return libroElegido;

    }

    public void setLibro(ArrayList<Libro> libros) {
        this.libros = libros;
    }

    public void setUsuarios(ArrayList<Usuario> usuarios) {
        this.usuarios = usuarios;
    }

    //
//    public void setPrestamos(Prestamo[] prestamos) {
//        this.prestamos = prestamos;
//    }
//
    public ArrayList<Libro> getLibros() {
        return libros;
    }

    //
    public ArrayList<Usuario> getUsuarios() {
        return usuarios;
    }
//
//    public Prestamo[] getPrestamos() {
//        return prestamos;
//    }

//    public void setLibro(String[] libro) {
//        this.libro = libro;
//    }
//
//    public void setUsuarios(String[] usuarios) {
//        this.usuarios = usuarios;
//    }
//
//    public void setPrestamos(Prestamo[] prestamos) {
//        this.prestamos = prestamos;
//    }
//
    public Libro buscarLibro(String nombre) {
        int i=0;
        boolean encontrado = false;
        Libro libro = null;

       while (i < libros.size() && !encontrado) {
            libro = libros.get(i);
            if (nombre.equals(libro.getNombre())){
                encontrado = true;
            } else {
                i++;
            }
        }
        if (!encontrado) {
            return null;
        } else {
            return libro;
        }
      }

//    public String[] getUsuarios() {
//        return usuarios;
//    }
//
//    public Prestamo[] getPrestamos() {
//        return prestamos;
//    }
    public void agregarUsuario(Usuario u) {
        usuarios.add(u);
    }
public Usuario buscarUsuario(String datos) {
        int i = 0;
        boolean encontrado = false;
        Usuario u = null;

        while (i < usuarios.size() && !encontrado) {
            u = usuarios.get(i);
            if (datos.equals(u.getNombre()+ ":" + u.getPassword())) {
                encontrado = true;
            } else {
                i++;
            }
        }
        if (!encontrado) {
            return null;
        } else {
            return u;
        }
    }

public void mostrarUsuarios() {
        System.out.println("************** LISTA DE USUARIOS **************\n");
        if (usuarios.size() <= 0) {
            EntradaSalida.mostrarString("Aun no se registrado Usuarios");
        } else {
            for (int i = 0; i < usuarios.size(); i++) {
                usuarios.get(i).mostrar();
            }
        }
    }

 
    
}
