/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject1;

import java.io.IOException;
import java.io.Serializable;


public class Administrador extends Usuario implements Prestable, Serializable {

    public Administrador(String password, String nombre) {
        super(password, nombre);
     
    }

    
    public void altaUsuarios(){}
    public void bajaUsuarios(){}
    public void editaUsuarios(){}
    public void modificarUsuarios(){}
    
    public void altaLibros(){}
    public void bajaLibro(){}
    public void editarLibro(){}
    public void modificarLibro(){}

    @Override
    boolean proceder(Biblioteca biblioteca) {
      //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        char op;
        boolean continuar = true;
        do {
            op = EntradaSalida.leerChar(
                    "**********************************************\nFUNCIONES DEL ADMINISTRADOR\n**********************************************\n"
                    + "1-   Dar de alta un Bibliotecario\n"
                    + "2-   Dar de alta un Lector\n"
                    + "3-   Eliminar Bibliotecario\n"
                    + "4-   Eliminar Lector\n"
                    + "5-   Listado de Usuarios\n"
                    + "6-   Agregar libro\n"
                    + "7-   Listado de Libros\n"
                    + "8-   Cambiar de Usuario\n"
                    + "9-   Salir del sistema");

            String b = "BIBLIOTECARIO";
            String l = "LECTOR";

            switch (op) {
                case '1':
                    registrarUsuarios(biblioteca, b);

                    break;
                case '2':
                    registrarUsuarios(biblioteca, l);

                    break;
                case '3':
                   // eliminarBibliotecarios(biblioteca, b);

                    break;
                case '4':
                  //  eliminarSocios(biblioteca, s);

                    break;
                case '5':
                  //  biblioteca.mostrarUsuarios();

                    break;
                case '6':
                  //  biblioteca.agregarLibro();

                    break;
                case '7':
                  //  biblioteca.mostrarLibros();

                    break;
                case '8':
                    continuar = true;

                    break;
                case '9':
                    continuar = false;

                    break;
                default:
                    EntradaSalida.mostrarString("ERROR: Opcion invalida");
                    op = '*';
            } //Fin switch
            if (op >= '1' && op <= '7') {
                try {
                    biblioteca.serializar("biblioteca.txt");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } while (op != '8' && op != '9'); //Fin while
        return continuar;

    }
    
    private void registrarUsuarios(Biblioteca b, String tipoU) {
        String username, password;
        username = EntradaSalida.leerString("ALTA DE " + tipoU + "\n" + "Ingrese nombre de usuario:");
        if (username.equals("")) {
            EntradaSalida.mostrarString("ERROR: usuario no valido");
        } else {
            password = EntradaSalida.leerString("Ingrese password:");
            if (password.equals("")) {
                EntradaSalida.mostrarString("ERROR: password no valida");
            } else {
                Usuario u = b.buscarUsuario(username + ":" + password);
                if (u != null) {
                    EntradaSalida.mostrarString("ERROR: " + username + "Ya se encuentra registrado en el sistema");
                } else {
                    if (tipoU.equals("BIBLIOTECARIO")) {
                        u = new Bibliotecario( password, username);
                        b.agregarUsuario(u);
                    } else if (tipoU.equals("LECTOR")) {
                        u = new Lector(password, username);
                         b.agregarUsuario(u);
                    }
                    EntradaSalida.mostrarString("Se ha incorporado un nuevo " + tipoU + " al sistema");
                }

            }
        }
    }



   
    
    
}
