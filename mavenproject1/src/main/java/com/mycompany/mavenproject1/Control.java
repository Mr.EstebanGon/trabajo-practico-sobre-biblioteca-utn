/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject1;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author maria
 */
public class Control {
    public void ejecutar(){
    Biblioteca biblioteca = new Biblioteca();
    boolean continuar;
     try {
            biblioteca = biblioteca.deSerializar("biblioteca.txt");
            continuar = EntradaSalida.leerBoolean("SISTEMA  BIBLIOTECA NACIONAL\nDesea ingresar?");
        } catch (Exception e) {
            String usuario = EntradaSalida.leerString("Arranque inicial del sistema.\n"
                    + "Sr(a) Administrador(a), ingrese su nombre de usuario:");
            if (usuario.equals("")) {
                throw new NullPointerException("ERROR: El usuario no puede ser nulo.");
            }
            String password = EntradaSalida.leerPassword("Ingrese su password:");
            if (password.equals("")) {
                throw new NullPointerException("ERROR: La password no puede ser nula.");
            }
            biblioteca.agregarUsuario(new Administrador(password, usuario ));
           
            try {
                biblioteca.serializar("biblioteca.txt");
                EntradaSalida.mostrarString("El arranque ha sido exitoso. Ahora se debe reiniciar el sistema...");
            } catch (IOException ex) {
            }
            continuar = false;
        }

        while (continuar) {
            String usuario = EntradaSalida.leerString("Ingrese el usuarioo:");
            String password = EntradaSalida.leerPassword("Ingrese la password:");

            Usuario u = biblioteca.buscarUsuario(usuario + ":" + password);

            if (u == null) {
                EntradaSalida.mostrarString("ERROR: La combinacion usuario/password ingresada no es valida.");
            } else {                
                    continuar = u.proceder(biblioteca);  // POLIMORFISMO!!!!                   
                   }
        }
    }
}

