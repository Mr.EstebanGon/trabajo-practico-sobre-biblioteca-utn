package com.mycompany.mavenproject1;

import java.io.Serializable;

/**
 *
 * @author esteb
 */
public abstract class Usuario extends Persona implements Serializable {
    //private Integer idUsuario;
    private static  int  idUsuario = 0;

    private String password;
   // private String tipo;

    public Usuario(String password, String nombre) {
        super(nombre);
        this.password = password;        
    }   
    

    
    public void setPassword(String password) {
        this.password = password;
    }

   

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public String getPassword() {
        return password;
    }

    
    // METODO IMPORTANTE PARA LOS 3 TIPOS DE USUARIOS
    abstract boolean proceder(Biblioteca biblioteca);

    void mostrar() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
