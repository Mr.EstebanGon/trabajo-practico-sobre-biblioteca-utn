package com.mycompany.mavenproject1;

public class Libro {
    private String nombre;
    private String Anio;
    private Autor autor;
    private String estado;
    private String categoria;
    private String editorial;
    private Integer canCopias;

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setAnio(String Anio) {
        this.Anio = Anio;
    }

    public void setAutor(Autor autor) {
        this.autor = autor;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public void setEditorial(String editorial) {
        this.editorial = editorial;
    }

    public void setCanCopias(Integer canCopias) {
        this.canCopias = canCopias;
    }

    public String getNombre() {
        return nombre;
    }

    public String getAnio() {
        return Anio;
    }

    public Autor getAutor() {
        return autor;
    }

    public String getEstado() {
        return estado;
    }

    public String getCategoria() {
        return categoria;
    }

    public String getEditorial() {
        return editorial;
    }

    public Integer getCanCopias() {
        return canCopias;
    }
    
    
}
